package gerber.org.login;

import android.content.ClipData;
import android.content.Intent;
import android.graphics.Color;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;
import android.support.v7.widget.Toolbar;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import activity.FragmentDrawer;


public class MainActivity extends ActionBarActivity {

    Button btnLogin,btnRegistrar;
    EditText txtUsuarios,txtPassw;
    private Toolbar mToolbar;
    private FragmentDrawer drawerFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        btnLogin = (Button) findViewById(R.id.btnLogin);
        btnRegistrar = (Button) findViewById(R.id.btnRegistrar);
        txtUsuarios = (EditText)findViewById(R.id.txtUsuario);
        txtPassw = (EditText)findViewById(R.id.txtPass);
        MenuItem it= (MenuItem)findViewById(R.id.Tema);




        btnRegistrar.setOnClickListener(new View.OnClickListener(){
            public void onClick (View view){
                try {
                    Intent myIntent = new Intent(view.getContext(), RegistrarActivity.class);

                    startActivityForResult(myIntent, 0);
                }catch (NullPointerException e){

                }

            }
        });







        btnLogin.setOnClickListener(new View.OnClickListener() {

            ManejadorUsuario  mj= ManejadorUsuario.getInstancia();
            public void onClick(View view) {
                    for(int posicion=0;posicion<mj.obtenerUsuarios().size();posicion++) {
                        if (mj.obtenerUsuarios().get(posicion).getNombreUsuario().equals(txtUsuarios.getText().toString()) && mj.obtenerUsuarios().get(posicion).getPass().equals(txtPassw.getText().toString())) {
                            Intent myIntent = new Intent(view.getContext(), PeliculaActivity.class);
                            startActivityForResult(myIntent, 0);


                        } else {
                            int cant = ManejadorUsuario.getInstancia().obtenerUsuarios().size();
                            Toast t = Toast.makeText(getApplicationContext(), "Ingrese bien los datos", Toast.LENGTH_LONG);
                            t.show();
                        }
                    }





            }
        });
    }




    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


}
