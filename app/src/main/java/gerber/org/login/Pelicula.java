package gerber.org.login;

/**
 * Created by oscar on 19/05/2015.
 */
public class Pelicula {
    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getGenero() {
        return genero;
    }

    public void setGenero(String genero) {
        this.genero = genero;
    }

    public Pelicula(String nombre, String genero) {
        this.nombre = nombre;
        this.genero = genero;
    }

    private  String nombre;
    private String genero;

}
